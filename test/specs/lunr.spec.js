/* eslint-disable no-unused-expressions */
import LunrSearch from '../../src/lunr'
import {fields} from '../../src/plugin/freshdesk'
import lunr from 'lunr'

describe('Lunr', function () {
  before('create the index', function () {
    this.docs = {
      '0': {
        id: 0,
        title: 'fight club',
        description_text: 'if it is your first night at fight club, you have to fight',
        meta_title: 'fight club 1999',
        meta_keywords: '',
        meta_description: '',
        tags: ''
      },
      '1': {
        id: 1,
        title: 'django unchained',
        description_text: 'jaime foxx is pissed off',
        meta_title: 'django unchained 2012',
        meta_keywords: '',
        meta_description: 'the "d" is silent',
        tags: 'tarantino'
      },
      '2': {
        id: 2,
        title: 'the matrix',
        description_text: 'neo has to do some stuff',
        meta_title: 'the matrix 1999',
        meta_keywords: 'wachowski',
        meta_description: 'still a better love story than twilight',
        tags: 'keanu reeves'
      }
    }

    this.index = lunr(builder => {
      builder.ref('id')
      fields.forEach(f => builder.field(f))
      Object.values(this.docs).forEach(doc => builder.add(doc))
    })
  })

  beforeEach('init the Lunr object', function () {
    this.lunrSearch = new LunrSearch(
      'srffoss',
      'srf.foss@gmail.com',
      FD_PASS
    )
  })

  describe('#search', function () {
    beforeEach('set the index to the test index', function () {
      this.lunrSearch.docs = this.docs
      return this.lunrSearch.buildIndex()
    })

    it('should return the correct results', function () {
      this.lunrSearch.search('fight club')[0].result.ref.should.equal('0')
      this.lunrSearch.search('django')[0].result.ref.should.equal('1')
      this.lunrSearch.search('tarantino')[0].result.ref.should.equal('1')
      this.lunrSearch.search('keanu')[0].result.ref.should.equal('2')
      this.lunrSearch.search('"d"')[0].result.ref.should.equal('1')
      this.lunrSearch.search('1999').length.should.equal(2)
    })
  })

  describe('#fetch', function () {
    this.timeout(10000)

    it('should fetch some documents', function () {
      return this.lunrSearch.fetch().then(docs => docs.length).should.eventually.equal(8)
    })
  })

  describe('#buildIndex', function () {
    it('should build the index', function () {
      return this.lunrSearch.buildIndex().then(index => {
        index.fields.should.eql(fields)
      })
    })

    it('should not fetch new documents', function () {
      const spy = sinon.spy(this.lunrSearch, 'fetch')
      this.lunrSearch.docs.a = {}
      return this.lunrSearch.buildIndex().then(() => {
        spy.should.not.have.been.called
      })
    })
  })

  describe('#importIndex', function () {
    this.timeout(10000)

    it('should restore the elements of the field vector', function () {
      this.lunrSearch.index = this.index

      return this.lunrSearch.exportIndex().then(json => {
        this.lunrSearch.importIndex(json)
        Object.keys(this.lunrSearch.index.fieldVectors).forEach(key => this.lunrSearch.index.fieldVectors[key].elements.should.eql(this.index.fieldVectors[key].elements))
      })
    })
  })

  describe('#exportIndex', function () {
    this.timeout(10000)

    it('should build the index and export it', function () {
      return this.lunrSearch.exportIndex()
    })
  })

  describe('#isSupported', function () {
    it('should return true', function () { // the test environment should always be ES6 compatible
      LunrSearch.isSupported().should.equal(true)
    })
  })
})
