import {fetch, plugin, fields, process} from '../../../src/plugin/freshdesk'

describe('plugin.freshdesk', function () {
  describe('#fetch', function () {
    this.timeout(10000)

    before('init info', function () {
      this.domain = 'srffoss'
      this.user = 'srf.foss@gmail.com'
      this.pass = FD_PASS
    })

    it('should fetch documents', function () {
      return fetch(this.domain, this.user, this.pass).then(docs => docs.length).should.eventually.equal(8)
    })
  })

  describe('#parse', function () {
    beforeEach('load the json articles', function () {
      this.articles = [{
        title: '0',
        description_text: 'fff',
        seo_data: {meta_title: '0'},
        tags: []
      }, {
        title: '1',
        tags: ['a 1', 'b', 'c']
      }]
    })

    it('should init all undefined fields', function () {
      let article = process(this.articles[0])
      article.tags.should.equal('')
      article.meta_keywords.should.equal('')
      article.meta_description.should.equal('')

      article = process(this.articles[1])
      article.meta_keywords.should.equal('')
      article.meta_description.should.equal('')
      article.meta_title.should.equal('')
      article.description_text.should.equal('')
    })

    it('should map seo_data', function () {
      process(this.articles[0]).meta_title.should.equal(this.articles[0].seo_data.meta_title)
    })

    it('should concatenate tags', function () {
      process(this.articles[1]).tags.should.equal('a 1 b c')
    })
  })
})
