import chai from 'chai'
import sinonChai from 'sinon-chai'
import chaiAsPromised from 'chai-as-promised'

chai.should()
chai.use(sinonChai)
chai.use(chaiAsPromised)

// require all test files (files that ends with .spec.js)
const testsContext = require.context('./specs', true, /\.spec$/)
testsContext.keys().forEach(testsContext)

// require all source files for coverage purposes
const srcContext = require.context('../src', true, /\/index\.js$/)
srcContext.keys().forEach(srcContext)
