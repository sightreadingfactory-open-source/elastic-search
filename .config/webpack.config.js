const path = require('path');
const baseConfig = require('./webpack.base.config');
const {merge} = require('lodash');

module.exports = merge(baseConfig, {
  entry: './index.js',
  output: {
    library: 'LunrSearch',
    path: path.resolve(__dirname, '..', 'dist'),
    filename: 'elastic-search.js'
  },
  context: path.resolve(__dirname, '..')
});
