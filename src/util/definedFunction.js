/**
 * Checks to see if a function is defined.
 *
 * @param  {?Function} func the function to check
 * @return {boolean}        whether or not the funciton is defined
 * @memberOf util
 * @function definedFunction
 */
export default function definedFunction (func) {
  return func !== undefined && func.constructor.name === 'Function'
}
