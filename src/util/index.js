import definedFunction from './definedFunction'
import buildResults from './buildResults'

/**
 * @namespace util
 */
export {definedFunction, buildResults}
