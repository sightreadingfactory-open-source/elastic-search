export default function buildResults (results, docs) {
  const ret = []
  if (results) {
    results.forEach(result => {
      const document = docs[result.ref]
      if (document) ret.push({document, result, source: document.documentSource})
    })
  }
  return ret
}
