import * as freshdesk from './freshdesk'

/**
 * Contains plugins that fetch and preprocess documents as well as define the fields to index within the documents.
 *
 * @namespace plugin
 */
export {freshdesk}
