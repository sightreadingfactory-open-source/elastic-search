elastic-search
==============

[![pipeline status](https://gitlab.com/sightreadingfactory-open-source/elastic-search/badges/master/pipeline.svg)](https://gitlab.com/sightreadingfactory-open-source/elastic-search/commits/master)
[![npm](https://img.shields.io/npm/v/lunr-elastic-search.svg)](https://www.npmjs.com/package/lunr-elastic-search)
[![npm](https://img.shields.io/npm/l/lunr-elastic-search.svg)](https://gitlab.com/sightreadingfactory-open-source/elastic-search/LICENSE)

Documentation
-------------

API documentation is available at https://sightreadingfactory-open-source.gitlab.io/elastic-search/docs.

Reports
-------

- Unit tests: https://sightreadingfactory-open-source.gitlab.io/elastic-search/reports/unit
- Coverage: https://sightreadingfactory-open-source.gitlab.io/elastic-search/reports/coverage

Installation
------------

For usage in the browser:

Latest version:

```html
<script src="https://unpkg.com/lunr-elastic-search"></script>
```

Specific version #.#.#:

```html
<script src="https://unpkg.com/lunr-elastic-search@#.#.#"></script>
```

This will create a global variable `LunrSearch` which is the class [`LunrSearch`](https://sightreadingfactory-open-source.gitlab.io/elastic-search/docs/LunrSearch.html)

Usage
-----

Refer to the [API docs](https://sightreadingfactory-open-source.gitlab.io/elastic-search/docs/LunrSearch.html)
